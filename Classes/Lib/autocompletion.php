<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

	$configPath = realpath(dirname(dirname(dirname(dirname(dirname($_SERVER["SCRIPT_FILENAME"]))))));
	$conf = (include_once $configPath.'/LocalConfiguration.php');

	$query = utf8_decode($_GET['term']);
	$onlypublic = $_GET['onlyPublic'];
	echo json_encode(getWordsAutocompletion($query, $conf['DB']['Connections']['Default'], $onlypublic));
	
	
	/**
	* get five words of each category to initialize the autocompletion
	*/
	function getWordsAutocompletion($query, $dbconf, $onlypublic){
		$port = 3306;

		$conn = new mysqli($dbconf['host'], $dbconf['user'], $dbconf['password'], $dbconf['dbname'], $port);
		if ($conn->connect_error) {
	       // die("Connection failed: " . $conn->connect_error);
		} 

		// get five words of all categories
		$result_array = array();
		
		if(strlen($query)<3){
			return $result_array;
		}
		
		// get all categories
		$sql_types = "SELECT DISTINCT type FROM tx_autocompletion";
		$result_types = $conn->query($sql_types);

		while($row_types = $result_types->fetch_assoc()) {
			$type = $row_types["type"];
			// set the names of the categories
			$key = $type;
			if($key == 'page') {
				$key = "Statische Seiten";
			}
			else if($key == 'publish_calendar') {
				$key = "News & Veranstaltungen";
			}
			else if($key == 'publish_geoobject') {
				$key = "Geodaten & Objekte";
			}
			else if($key == 'publish_servicebw' || $key == 'publish_baybw') {
				$key = "Rathaus-Dienste";
			}
			else if($key == 'pdf') {
				$key = "Dokumente & PDFs";
			}
            else if($key == 'publish_participation') {
                $key = "Beteiligung & Interaktion";
            }
			else if($key == 'publish_board') {
                $key = "Ausschüsse";
            }
            else if($key == 'publish_conference' || $key == 'publish_conferenceFile') {
                $key = "Konferenzen";
            }
            else if($key == 'publish_party') {
                $key = "Parteien";
            }
            else if($key == 'publish_person') {
                $key = "Personen";
            }

			// get five words for the category
			$sql = "SELECT DISTINCT word FROM tx_autocompletion WHERE (public=1 OR public=". $onlypublic .") AND type='" . $type . "' AND word LIKE '" .$query. "%' LIMIT 5";
			$result = $conn->query($sql);

			while($row = $result->fetch_assoc()) {
				array_push($result_array, array('label' => $row["word"], 'category' => $key));			}
		}
		return $result_array;
	}
?>